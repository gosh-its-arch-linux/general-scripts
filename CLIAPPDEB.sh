#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
    echo "Please run as sudo or as root."
    exit 1
fi

while true; do
    clear
    echo "Thank you for using Gosh-Its-Debian CLI App Store"
    echo "This script will modify installed packages system-wide; use at own risk!"
    echo ""
    echo "1) Application Search"
    echo "2) Show Installed Applications"
    echo "3) Add an Application"
    echo "4) Remove an Application"
    echo "5) Add Multiple Applications"
    echo "6) Remove Multiple Applications"
    echo "7) User Text File"
    echo "8) Exit"
    read -p "Select an option: " choice

    case $choice in
        1)
            read -p "Enter package name for search: " package_name
            echo "Searching for $package_name..."
            apt search "$package_name"
            read -p "Press any key to return to the main menu..."
            ;;

        2)
            echo "Installed Applications:"
            dpkg --get-selections | grep -v deinstall
            read -p "Press any key to continue..."
            ;;

        3)
            read -p "Enter the name of the application you'd like to add (e.g., firefox): " app_name
            apt install -y "$app_name"
            ;;

        4)
            read -p "Enter the name of the application you'd like to remove: " app_name
            apt remove -y "$app_name"
            ;;

        5)
            echo "Enter names of applications you'd like to add, separated by spaces (e.g., firefox vlc): "
            read -a app_names
            apt install -y "${app_names[@]}"
            ;;

        6)
            echo "Enter names of applications you'd like to remove, separated by spaces (e.g., firefox vlc): "
            read -a app_names
            apt remove -y "${app_names[@]}"
            ;;

        7)
            if [[ ! -f "./packages.txt" ]]; then
                echo "Error: Can't find packages.txt in the current directory!"
                read -p "Press any key to return to the main menu..."
                continue
            fi

            echo "Would you like to:"
            echo "1) Add applications from packages.txt"
            echo "2) Remove applications listed in packages.txt"
            echo "3) Return to Main Menu"
            read -p "Enter choice (1/2/3): " txt_choice

            case $txt_choice in
                1)
                    xargs apt install -y < "./packages.txt"
                    ;;

                2)
                    xargs apt remove -y < "./packages.txt"
                    ;;

                3)
                    echo "Returning to the main menu."
                    ;;

                *)
                    echo "Invalid choice!"
                    ;;
            esac
            ;;

        8)
            echo "Goodbye!"
            exit 0
            ;;

        *)
            echo "Invalid option!"
            ;;
    esac
done
