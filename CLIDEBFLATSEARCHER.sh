#!/bin/bash

# Create or clear the results.txt file
echo -n "" > results.txt

# Function to print to both terminal and file
print_output() {
    echo "$1"
    echo "$1" >> results.txt
}

# Function to check Debian repo for an app
check_debian() {
    local app="$1"
    apt-cache show "$app" &> /dev/null
    if [ $? -eq 0 ]; then
        print_output "$app is available in Debian repos."
    else
        print_output "$app is NOT available in Debian repos."
    fi
}

# Function to check Flathub for an app
check_flathub() {
    local app="$1"
    flatpak search "$app" | grep -q "^$app"
    if [ $? -eq 0 ]; then
        print_output "$app is available on Flathub."
    else
        print_output "$app is NOT available on Flathub."
    fi
}

# Iterate over all apps given as arguments
for app in "$@"; do
    check_debian "$app"
    check_flathub "$app"
    print_output "----"
done

print_output "Results saved to results.txt."
